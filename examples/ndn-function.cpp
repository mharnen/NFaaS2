/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2011-2015  Regents of the University of California.
 *
 * This file is part of ndnSIM. See AUTHORS for complete list of ndnSIM authors and
 * contributors.
 *
 * ndnSIM is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * ndnSIM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ndnSIM, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 **/

// ndn-simple.cpp

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/ndnSIM-module.h"
#include "ns3/topology-read-module.h"
#include "model/ndn-l3-protocol.hpp"
#include "model/ndn-l3-protocol.hpp"
#include "helper/ndn-fib-helper.hpp"
#include "model/ndn-net-device-transport.hpp"
#include "model/ndn-global-router.hpp"


#define FREQUENCY "1"
#define KERNELS 1
#define TIME 2
#define CLOUD 1
namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Simulation");

//int sources[] = {111, 112, 132, 148, 180, 184, 202, 220, 226, 229, 230, 240, 244, 246, 262, 272, 274, 279, 282, 290, 291, 313, 314, 35, 43, 47, 56, 66, 71, 80, 86};
//int sources[] = {0, 1, 3, 4, 6, 7};
int sources[] = {0};


int
main(int argc, char* argv[])
{
  // setting default parameters for PointToPoint links and channels
  Config::SetDefault("ns3::PointToPointNetDevice::DataRate", StringValue("100Mbps"));
  Config::SetDefault("ns3::PointToPointChannel::Delay", StringValue("1ms"));
  Config::SetDefault("ns3::DropTailQueue::MaxPackets", StringValue("50"));

  // Read optional command-line parameters (e.g., enable visualizer with ./waf --run=<> --visualize
  std::string format ("Inet");
//  std::string input ("src/topology-read/examples/task-topo.txt");
  std::string input ("src/topology-read/examples/small_topo.txt");
//  std::string input ("src/topology-read/examples/Inet_small_toposample.txt");
//  std::string format ("Rocketfuel");
//  std::string input ("src/topology-read/examples/RocketFuel_toposample_1239_weights.txt");

  double time = TIME;
  int kernels = KERNELS;
  string frequency = FREQUENCY;
  string size = "100";
  string deadline = "150";
  CommandLine cmd;

  cmd.AddValue ("format", "Format to use for data input [Orbis|Inet|Rocketfuel].", format);
  cmd.AddValue ("input", "Name of the input file.", input);
  cmd.AddValue ("time", "Time of the simulation.", time);
  cmd.AddValue ("kernels", "Number of kernels.", kernels);
  cmd.AddValue ("frequency", "Number of kernels.", frequency);
  cmd.AddValue ("size", "Task size", size);
  cmd.AddValue ("deadline", "Task deadline", deadline);
  cmd.Parse(argc, argv);

  NS_LOG_INFO("Reading file: " << input << " format: " << format);


  TopologyReaderHelper topoHelp;
  topoHelp.SetFileName (input);
  topoHelp.SetFileType (format);
  Ptr<TopologyReader> inFile = topoHelp.GetTopologyReader ();

  NodeContainer nodes;

  if (inFile != 0){
    nodes = inFile->Read ();
  }

  if (inFile->LinksSize () == 0){
    NS_LOG_ERROR ("Problems reading the topology file. Failing.");
    return -1;
  }


  TopologyReader::ConstLinksIterator lit;
  PointToPointHelper p2p;
  NS_LOG_INFO("Links:");
  for(lit = inFile->LinksBegin(); lit != inFile->LinksEnd(); lit++){
    NS_LOG_INFO("Link: " << lit->GetFromNode()->GetId() << "|" << lit->GetToNode()->GetId());
    p2p.Install(lit->GetFromNode(), lit->GetToNode());
  }

//  return 0;
  // Creating nodes
//  nodes.Create(4);

  // Connecting nodes using two links
 /* p2p.Install(nodes.Get(0), nodes.Get(1));
  p2p.Install(nodes.Get(0), nodes.Get(2));
  p2p.Install(nodes.Get(1), nodes.Get(3));
  p2p.Install(nodes.Get(2), nodes.Get(3));*/

  // Install NDN stack on all nodes
  ndn::StackHelper ndnHelper;
  ndnHelper.SetDefaultRoutes(true);
  ndnHelper.InstallAll();


  // Choosing forwarding strategy
  ndn::StrategyChoiceHelper::InstallAll("/", "/localhost/nfd/strategy/best-route2");
  ndn::StrategyChoiceHelper::InstallAll("/exec", "/localhost/nfd/strategy/function");
  // Installing global routing interface on all nodes

  // Install NDN applications
  std::string prefix_cloud = "/cloud/";
  std::string prefix_exec = "/exec/";
  // Installing applications

  // Consumer
  ndn::AppHelper consumerHelper("ns3::ndn::ConsumerTask");
  // Consumer will request /prefix/0, /prefix/1, ...
  //
  consumerHelper.SetAttribute("Frequency", StringValue(frequency)); // 1 interests a second
  consumerHelper.SetAttribute("TaskSize", StringValue(size)); // 1 interests a second
  consumerHelper.SetAttribute("TaskDeadline", StringValue(deadline)); // 1 interests a second
  consumerHelper.SetAttribute("StartTime", StringValue("0s"));
  //consumerHelper.SetAttribute("StopTime", StringValue("1s"));
  consumerHelper.SetAttribute("Randomize", StringValue("none"));
  //consumerHelper.SetAttribute("StopTime", StringValue("3s"));
  int counter = 0;
  for(int source: sources){
	  for(int i = 1; i <= kernels; i++){
		std::stringstream ss;
		ss << "/exec/example/" << counter;
  		consumerHelper.SetPrefix(ss.str());
  		ss.str(std::string());
  		ss << 0 /*counter*/ << "s";
     	consumerHelper.SetAttribute("StartTime", StringValue(ss.str()));
     	ss.str(std::string());
     	/*ss << counter+1 << "s";
     	consumerHelper.SetAttribute("StopTime", StringValue(ss.str()));*/
	  	consumerHelper.Install(nodes.Get(source));
	  	Ptr<ndn::L3Protocol> ndn = nodes.Get(source)->GetObject<ndn::L3Protocol>();
	  	ndn->setCaching(false);
	  	counter++;
	  }

  }
  nodes.Get(CLOUD)->GetObject<ndn::L3Protocol>()->setCloud(true);


  /*consumerHelper.SetPrefix(prefix1);
  consumerHelper.SetAttribute("Frequency", StringValue("400")); // 1 interests a second
  consumerHelper.SetAttribute("StartTime", StringValue("5s"));
  //consumerHelper.SetAttribute("StopTime", StringValue("4s"));
 consumerHelper.Install(nodes.Get(0));                        // first node*/
  
/*  consumerHelper.SetPrefix(prefix1);
  consumerHelper.SetAttribute("Frequency", StringValue("400")); // 1 interests a second
  consumerHelper.SetAttribute("StartTime", StringValue("10s"));
  //consumerHelper.SetAttribute("StopTime", StringValue("4s"));
  consumerHelper.Install(nodes.Get(0));                        // first node*/


  // Add /prefix origins to ndn::GlobalRouter
  ndn::GlobalRoutingHelper ndnGlobalRoutingHelper;
  ndnGlobalRoutingHelper.InstallAll();
  /*ndnGlobalRoutingHelper.AddOrigins(prefix_exec, nodes.Get(1));
  ndnGlobalRoutingHelper.AddOrigins(prefix_exec, nodes.Get(2));*/
  for(ns3::Ptr<ns3::Node> node: nodes){
	  ndnGlobalRoutingHelper.AddOrigins(prefix_exec, node);
  }
  ndnGlobalRoutingHelper.AddOrigins(prefix_cloud, nodes.Get(CLOUD));

  // Calculate and install FIBs
  ndn::GlobalRoutingHelper::CalculateRoutes();

  Simulator::Stop(Seconds(time));

  Simulator::Run();
  Simulator::Destroy();

  return 0;
}

} // namespace ns3

int
main(int argc, char* argv[])
{
  return ns3::main(argc, argv);
}
